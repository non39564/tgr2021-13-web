<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use Illuminate\Support\Arr;

class ItemController extends Controller
{
    public function index(){
        $item = Item::orderby('created_at', 'desc')->get();
        return view('index' ,["items"=> $item]);
    }

    public function kind(){
        $item = Item::groupBy('found')->get(['found']);
        return view('kind', ["kind" => Arr::pluck($item, 'found')]);
    }

    public function list(Request $request, $kind){
        $item = Item::where('found', $kind)->orderby('created_at','desc')->get();
        return view('kindtype',["items"=> $item]);
        //return response()->json($item);
    }

    public function push(Request $request){
        $input = $request->all();
        $item = Item::create($input);
    }

    public function hi(){
        return response()->json('SA-TTT');
    }

    public function lime(){
        $item = Item::where('verified','=',1)->get(); 
        return view('lime', ["lime"=> $item]);
    }

    public function search(Request $request){
        $search = $request->get('search');
        $items = Item::where('found', 'LIKE', '%' .$search. '%')->get(  );
        return view('welcome', ['Item' => $items]);
    }
    
}