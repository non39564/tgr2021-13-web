<table>
<tr>
<th>timestamp</th>
<th>found</th>
<th>qty</th>
<th>Is lime</th>
</tr>
@forelse  ($items as $item)
<tr>
    <td>{{ $item->created_at }}</td>
    <td>{{ $item->found }}</td>
    <td>{{ $item->qty }}</td>
    @if ($item->verified == 1)
        <td>มะนาว</td>
    @else
        <td>ไม่ใช่มะนาว</td>
    @endif

</tr>
@empty
<tr><td>No item</td></tr>
@endforelse
</table>

<a href="{{ route('kind') }}">kind page</a>