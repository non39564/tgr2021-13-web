<div class="col-md-4">
    <form action="/welcome" method="get">
        <div class="input-group">
            <input type="search" name="search" class="form-control">
            <span class="input-group-prepend">
                <button type="submit" class="btn btn-primary">Search</button>
            </span>
        </div>
    </form>
</div>
<table>
<tr>
<th>timestamp</th>
<th>found</th>
<th>qty</th>
</tr>
@forelse ($Item as $item)
<tr>
    <td>{{ $item->created_at }}</td>
    <td>{{ $item->found }}</td>
    <td>{{ $item->qty }}</td>

</tr>
@empty
<tr><td>No item</td></tr>
@endforelse
</table>

<a href="{{ route('kind') }}">kind page</a>