<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            [
            'found' => "Apple",
            'qty' => 1,
            ],
            [
            'found' => "Papaya",
            'qty' => 1,
            ],
            [
            'found' => "Orange",
            'qty' => 2,
            ],
            [
            'found' => "Banana",
            'qty' => 2,
            ]]);
    }
}