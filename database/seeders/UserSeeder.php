<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        DB::table('users')->insert([
            [
            'name' => 'ธเนษฐ ขันอาสา',
            'email' => 'thanet@siam-u.ac.th',
            'password' => 'Topgun2021',
            ],
            [
            'name' => 'อัญชลี ผลาเลิศ',
            'email' => 'anchalee@siam-u.ac.th',
            'password' => 'Topgun2021',
            ],
            [
            'name' => 'ธรรมนูญ แก้ววิฑูรย์',
            'email' => 'thammanoon@siam-u.ac.th',
            'password' => 'Topgun2021',
            ],
            [
            'name' => 'ศราวุธ อ่อนศรี',
            'email' => 'sarawut@siam-u.ac.th',
            'password' => 'Topgun2021',
            ],
            [
            'name' => 'ธเนษฐ ศิริบูรณ์',
            'email' => 'thanets@siam-u.ac.th',
            'password' => 'Topgun2021',]
        ]);
    }
}